<?xml version="1.0" encoding="utf-8"?>
<!--
~ Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
~
~ Entgra (pvt) Ltd. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied.  See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>org.wso2.carbon.devicemgt</groupId>
        <artifactId>application-mgt-feature</artifactId>
        <version>4.1.11-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>org.wso2.carbon.device.application.mgt.server.feature</artifactId>
    <packaging>pom</packaging>
    <version>4.1.11-SNAPSHOT</version>

    <name>WSO2 Carbon - Application Management Server Feature</name>
    <url>https://entgra.io</url>
    <description>This feature contains the core bundles required for Back-end Application Management functionality
    </description>

    <dependencies>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.device.application.mgt.common</artifactId>
        </dependency>
        <dependency>
            <groupId>org.wso2.carbon.devicemgt</groupId>
            <artifactId>org.wso2.carbon.device.application.mgt.core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.googlecode.plist</groupId>
            <artifactId>dd-plist</artifactId>
            <version>${googlecode.plist.version}</version>
        </dependency>
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>${commons-validator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.cxf</groupId>
            <artifactId>cxf-rt-frontend-jaxrs</artifactId>
            <version>${cxf.version}</version>
        </dependency>

        <!--<dependency>-->
            <!--<groupId>javax.servlet.jsp</groupId>-->
            <!--<artifactId>javax.servlet.jsp-api</artifactId>-->
        <!--</dependency>-->
        <!--<dependency>-->
            <!--<groupId>org.wso2.orbit.org.scannotation</groupId>-->
            <!--<artifactId>scannotation</artifactId>-->
        <!--</dependency>-->
        <!--<dependency>-->
            <!--<groupId>org.wso2.carbon.devicemgt</groupId>-->
            <!--<artifactId>org.wso2.carbon.device.application.mgt.api.feature</artifactId>-->
            <!--<type>zip</type>-->
        <!--</dependency>-->
        <!--<dependency>-->
            <!--<groupId>org.wso2.carbon.devicemgt</groupId>-->
            <!--<artifactId>org.wso2.carbon.device.application.mgt.auth.handler.feature</artifactId>-->
            <!--<type>zip</type>-->
        <!--</dependency>-->
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy-resources</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>src/main/resources</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>resources</directory>
                                    <includes>
                                        <include>build.properties</include>
                                        <include>p2.inf</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.wso2.maven</groupId>
                <artifactId>carbon-p2-plugin</artifactId>
                <version>${carbon.p2.plugin.version}</version>
                <executions>
                    <execution>
                        <id>p2-feature-generation</id>
                        <phase>package</phase>
                        <goals>
                            <goal>p2-feature-gen</goal>
                        </goals>
                        <configuration>
                            <id>org.wso2.carbon.device.application.mgt.server</id>
                            <propertiesFile>../../../features/etc/feature.properties</propertiesFile>
                            <adviceFile>
                                <properties>
                                    <propertyDef>org.wso2.carbon.p2.category.type:server</propertyDef>
                                    <propertyDef>org.eclipse.equinox.p2.type.group:false</propertyDef>
                                </properties>
                            </adviceFile>
                            <!--<includedFeatures>-->
                                <!--<includedFeatureDef>org.wso2.carbon.devicemgt:org.wso2.carbon.device.application.mgt.api.feature:${carbon.device.mgt.version}</includedFeatureDef>-->
                                <!--<includedFeatureDef>org.wso2.carbon.devicemgt:org.wso2.carbon.device.application.mgt.auth.handler.feature:${carbon.device.mgt.version}</includedFeatureDef>-->
                            <!--</includedFeatures>-->
                            <bundles>
                                <bundleDef>
                                    org.wso2.carbon.devicemgt:org.wso2.carbon.device.application.mgt.common:${carbon.device.mgt.version}
                                </bundleDef>
                                <bundleDef>
                                    org.wso2.carbon.devicemgt:org.wso2.carbon.device.application.mgt.core:${carbon.device.mgt.version}
                                </bundleDef>
                                <bundleDef>
                                    com.googlecode.plist:dd-plist:${googlecode.plist.version}
                                </bundleDef>
                                <bundleDef>
                                    commons-validator:commons-validator:${commons-validator.version}
                                </bundleDef>
                                <!--<bundleDef>-->
                                    <!--org.apache.cxf:cxf-rt-frontend-jaxrs:${cxf.version}-->
                                <!--</bundleDef>-->
                                <!--<bundleDef>javax.servlet.jsp:javax.servlet.jsp-api</bundleDef>-->
                                <!--<bundleDef>org.wso2.orbit.org.scannotation:scannotation:${scannotation.version}</bundleDef>-->
                            </bundles>
                            <importBundles>
                            </importBundles>
                            <!--<importFeatures>-->
                                <!--<importFeatureDef>org.wso2.carbon.core.server:${carbon.kernel.version}</importFeatureDef>-->
                            <!--</importFeatures>-->
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <properties>
        <commons-validator.version>1.6</commons-validator.version>
    </properties>
</project>
